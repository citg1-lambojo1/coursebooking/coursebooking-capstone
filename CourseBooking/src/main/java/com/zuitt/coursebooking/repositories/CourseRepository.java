package com.zuitt.coursebooking.repositories;

import com.zuitt.coursebooking.models.Course;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends CrudRepository<Course, Object> {

}
