package com.zuitt.coursebooking.services;

import com.zuitt.coursebooking.models.User;
import com.zuitt.coursebooking.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

    // Create user
    public void createUser(User user) {
        userRepository.save(user);
    }

    // Get users
    public Iterable<User> getUsers() {
        return userRepository.findAll();
    }

    // Delete user
    public ResponseEntity deleteUser(int id) {
        userRepository.deleteById(id);
        return new ResponseEntity<>("User deleted successfully", HttpStatus.OK);
    }

    // Update user
    public ResponseEntity updateUser(int id, User user) {
        User userForUpdating = userRepository.findById(id).get();

        userForUpdating.setUsername(user.getUsername());
        userForUpdating.setPassword(user.getPassword());
        userRepository.save(userForUpdating);
        return new ResponseEntity<>("User updated successfully", HttpStatus.OK);

    }

    //    Find user by username
    public Optional<User> findByUsername(String username){
        return Optional.ofNullable(userRepository.findByUsername(username));
    }

}
