package com.zuitt.coursebooking.services;

import com.zuitt.coursebooking.models.Course;
import org.springframework.http.ResponseEntity;

public interface CourseService {

    //    Create a Course
    void createCourse(String stringToken, Course course);

    //    Viewing all Courses
    Iterable<Course> getCourses();

    // Delete a Course
    ResponseEntity deleteCourse(int id, String stringToken);

    // Update a Course
    ResponseEntity updateCourse(int id, String stringToken, Course course);

}
