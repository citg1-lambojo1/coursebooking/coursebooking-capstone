package com.zuitt.coursebooking.services;

import com.zuitt.coursebooking.config.JwtToken;
import com.zuitt.coursebooking.models.Course;
import com.zuitt.coursebooking.models.User;
import com.zuitt.coursebooking.repositories.CourseRepository;
import com.zuitt.coursebooking.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl implements  CourseService{

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    //    Create course
    public void createCourse(String stringToken, Course course){
        User user = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Course newCourse = new Course();
        newCourse.setName(course.getName());
        newCourse.setDescription(course.getDescription());
        newCourse.setPrice(course.getPrice());
        newCourse.setUser(user);
        courseRepository.save(newCourse);
    }

    //    Get All Courses
    public Iterable<Course> getCourses(){
        return courseRepository.findAll();
    }

    // Delete a Course
    public ResponseEntity deleteCourse(int id, String stringToken) {

        Course courseForDeleting = courseRepository.findById(id).get();
        String postAuthorName = courseForDeleting.getUser().getUsername();
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUserName.equals(postAuthorName)) {
            courseRepository.deleteById(id);
            return new ResponseEntity<>("Course deleted successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to delete this course.", HttpStatus.UNAUTHORIZED);
        }

    }


    // Update a Course
    public ResponseEntity updateCourse(int id, String stringToken, Course course) {

        Course courseForUpdating = courseRepository.findById(id).get();
        String postAuthorName = courseForUpdating.getUser().getUsername();
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUserName.equals(postAuthorName)) {

            courseForUpdating.setDescription(course.getDescription());
            courseForUpdating.setName(course.getName());
            courseForUpdating.setPrice(course.getPrice());
            courseRepository.save(courseForUpdating);
            return new ResponseEntity<>("Course updated successfully", HttpStatus.OK);

        } else {

            return new ResponseEntity<>("You are not authorized to edit this course.", HttpStatus.UNAUTHORIZED);

        }

    }


}
